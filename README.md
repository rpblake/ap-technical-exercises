### Technical Exercises

This project is built with Maven and targets Java 8 (inherited from the GildedRose GitHub project). In the `src` directory,
you will find two packages, `gildedrose` and `operator`. As the name would suggest, each of these packages represents the
source code for one of the required technical exercises. 

#### Gilded Rose Refactor

The refactored source code is [here](src/main/java/com/example/rb/gildedrose)

The general approach has been to introduce the concept of an `ItemAdjuster` interface that understands
how to adjust the `sellIn` and `quality` of an Item. There is a default implementation and then Item-specific
implementations to capture the different ways in which Items change their `sellIn` and `quality` values.

The mapping of an `ItemAdjuster` to an Item is performed by the `ItemAdjustFactory`. In addition, determining
whether or not an Item is `conjured` is delegated to the `ConjuredItemIdentifier` for which there is a 
simple default implementation thus far.

Finally, the `GildedRose` `updateQuality` method has been refactored to iterate over the list of Items and
apply the correct `ItemAdjuster`. After adjusting the `sellIn` and `quality` of an Item, `GildedRose` sanity
checks the `quality` value to ensure that is does not exceed the min/max thresholds of 0 and 50 respectively.

The solution has been unit tested fully [here](src/test/java/com/example/rb/gildedrose) with a combination of JUnit 5
and Mockito.

### Operator

Our Operator looks for instances of the `PodDeployer` CRD. The CRD definition is available [here](src/main/resources/crds/pod-deployer-crd.yml).

When you create an instance of the `PodDeployer` Custom Resource, you can specify two attributes:

* The number of replicas you would like to run
* The container image to be run in your Pod

As an example:

```yaml
---
kind: PodDeployer
apiVersion: rb.example.com/v1beta1
metadata:
  name: anexamplepoddeployer
spec:
  podCount: 2
  podImage: nginx:1.14.2
```

Here I am requesting two Pods, which are using the image `nginx:1.14.2`.

When the `PodDeployer` Operator creates the Pods, it delegates this to a `Deployment` resource. It does this so that
it does not have to concern itself with lifecycle management of the Pods, instead relying on the default Kubernetes controller
for `Deployment` resources to do this on its behalf.

Additionally, the `PodDeployer` Operator does not explicitly delete the `Deployment` resources it creates. Instead, it sets
the [OwnerReference](https://kubernetes.io/docs/concepts/workloads/controllers/garbage-collection) for the `Deployment`
to the `PodDeployer` Customer Resource instance. This means that when the `PodDeployer` Custom Resource instance is deleted,
Kubernetes automatically handles the cascading delete to the `Deployment` as part of its in-built garbage collection.  

The Operator code makes use of the [Fabric 8 K8S client](https://github.com/fabric8io/kubernetes-client) to introduce type-safe POJO representations of our `PodDeployer`
resources and these are marshalled to/from YAML using Jackson. The assembly of the Operator is handled by Spring.

Reconciles are executed on a bounded set of Threads. The number of Threads can be configured via environment configuration
set on the `Deployment` for the Operator at runtime. 

The Operator is designed to be Namespace-scoped.

The main reconcile loop for the Operator is implemented in the following [Class](src/main/java/com/example/rb/operator/PodDeployerOperator.java).

#### How the Operator would run

The JAR that is produced by this project would be built into a Container image and then published to a Container Registry
e.g. hub.docker.com. The Operator itself would be deployed as a `Deployment` or using the Operator Lifecycle Manager framework.
Permissions for the Operator would be managed via `ClusterRoles` and `ClusterRoleBinding` of which I've given an example
[here](src/main/resources/k8s/cluster-role.yml) and [here](src/main/resources/k8s/cluster-role-binding.yml). 

#### Testing

For the sake of timeliness I have not fully unit tested the Operator, but it would be easily achievable to do so with typical
unit testing and mocking frameworks.

