package com.example.rb.gildedrose;

import com.example.rb.gildedrose.adjusters.ItemAdjuster;
import com.example.rb.gildedrose.factory.ItemAdjusterFactory;

import static java.util.Arrays.stream;

class GildedRose {

    public static final int MIN_ITEM_QUALITY = 0;

    public static final int MAX_ITEM_QUALITY = 50;

    Item[] items;

    private final ItemAdjusterFactory itemAdjusterFactory;

    public GildedRose(Item[] items, ItemAdjusterFactory itemAdjusterFactory) {
        this.items = items;
        this.itemAdjusterFactory = itemAdjusterFactory;
    }

    /*
        Ensure that we enforce our min item quality of zero and max quality of 50. ItemAdjusters can't
        be trusted!
     */
    private void sanityCheckQualityAndSellInForItem(Item item) {
        if (item.quality < MIN_ITEM_QUALITY) {
            item.quality = MIN_ITEM_QUALITY;
        }

        if (item.quality > MAX_ITEM_QUALITY) {
            item.quality = MAX_ITEM_QUALITY;
        }
    }

    private void adjustItemQuality(Item item) {
        ItemAdjuster adjuster = itemAdjusterFactory.getAdjusterFor(item);
        adjuster.adjustItemQuality(item);
    }

    private void adjustAndSanityCheckItem(Item item) {
        /*
            Sulfuras is a legendary item and never changes value or has to be sold!
         */
        if(!ItemNames.SULFURAS_HAND_OF_RAGNAROS.equals(item.name)) {
            adjustItemQuality(item);
            sanityCheckQualityAndSellInForItem(item);
        }
    }

    public void updateQuality() {
        stream(items).forEach(this::adjustAndSanityCheckItem);
    }
}