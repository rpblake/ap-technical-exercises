package com.example.rb.gildedrose;

/**
 * Some constants for the names of our well-known inventory items.
 */
public final class ItemNames {

    public static final String AGED_BRIE = "Aged Brie";

    public static final String BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";

    public static final String SULFURAS_HAND_OF_RAGNAROS = "Sulfuras, Hand of Ragnaros";

    public static final String CONJURED_MANA_CAKE = "Conjured Mana Cake";

    private ItemNames() {

    }
}
