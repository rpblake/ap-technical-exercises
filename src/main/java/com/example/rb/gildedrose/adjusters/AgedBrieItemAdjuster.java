package com.example.rb.gildedrose.adjusters;

import com.example.rb.gildedrose.Item;

/**
 * ItemAdjuster implementation for our Aged Brie Stock
 */
public class AgedBrieItemAdjuster extends DefaultItemAdjuster {

    /*
        Aged Brie actually increases in quality the older it gets.
     */
    protected int getNewQuality(Item item) {
        return item.quality + 1;
    }
}