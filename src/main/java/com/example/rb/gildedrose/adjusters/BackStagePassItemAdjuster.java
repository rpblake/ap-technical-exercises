package com.example.rb.gildedrose.adjusters;

import com.example.rb.gildedrose.Item;

/**
 * ItemAdjuster for our Back Stage Pass stock.
 */
public class BackStagePassItemAdjuster extends DefaultItemAdjuster {

    @Override
    protected int getNewQuality(Item item) {

        if (item.sellIn == 0) {
            return 0;
        }

        if (item.sellIn <= 10 && item.sellIn > 5) {
            return item.quality + 2;
        } else if (item.sellIn <= 5 && item.sellIn > 0) {
            return item.quality + 3;
        } else {
            return item.quality - 1;
        }
    }
}