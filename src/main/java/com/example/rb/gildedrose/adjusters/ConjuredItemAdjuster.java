package com.example.rb.gildedrose.adjusters;

import com.example.rb.gildedrose.Item;

/**
 * ItemAdjuster for ConjuredItems.
 */
public class ConjuredItemAdjuster extends DefaultItemAdjuster {

    /*
        ConjuredItems degrade twice as fast as normal items.
     */
    @Override
    protected int getNewQuality(Item item) {
        return item.quality - (getDefaultQualityReduction(item) * 2);
    }
}
