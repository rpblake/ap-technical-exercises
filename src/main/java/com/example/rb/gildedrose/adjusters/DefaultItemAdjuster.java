package com.example.rb.gildedrose.adjusters;

import com.example.rb.gildedrose.Item;

import java.util.Objects;

/**
 * The default item adjuster implementation. Item sell in reduces by 1 each time. Quality reduces by 1 until the sell in
 * is zero, then quality reduces by 2.
 */
public class DefaultItemAdjuster implements ItemAdjuster {

    /**
     * Bt default, Sell In reduces by 1
     */
    public static final int DEFAULT_SELL_IN_REDUCTION = 1;

    /**
     * By default, quality reduces by 1
     */
    public static final int DEFAULT_QUALITY_REDUCTION = 1;

    /**
     * By default, an items quality reduces by two if the sell in has reached zero
     */
    public static final int SELL_BY_DATE_PASSED_QUALITY_REDUCTION = 2;

    @Override
    public final void adjustItemQuality(Item item) {
        Objects.requireNonNull(item, "Cannot adjust the quality of a null item");
        item.sellIn = getNewSellIn(item);
        item.quality = getNewQuality(item);
    }

    protected int getNewSellIn(Item item) {
        return item.sellIn - DEFAULT_SELL_IN_REDUCTION;
    }

    protected final int getDefaultQualityReduction(Item item) {
        return item.sellIn > 0 ? DEFAULT_QUALITY_REDUCTION : SELL_BY_DATE_PASSED_QUALITY_REDUCTION;
    }

    protected int getNewQuality(Item item) {
        return item.quality - getDefaultQualityReduction(item);
    }
}
