package com.example.rb.gildedrose.adjusters;

import com.example.rb.gildedrose.Item;

/**
 * Interface that captures the ability to adjust the quality of any item within our inventory.
 */
public interface ItemAdjuster {

    /**
     * Adjust the quality of the specified item.
     * @param item - The item to adjust the quality for.
     */
    void adjustItemQuality(Item item);
}
