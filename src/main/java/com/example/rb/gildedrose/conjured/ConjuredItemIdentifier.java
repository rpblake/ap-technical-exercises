package com.example.rb.gildedrose.conjured;

import com.example.rb.gildedrose.Item;

/**
 * An interface that lets us determine if an item is from our new supplier of conjured items.
 */
public interface ConjuredItemIdentifier {

    /**
     * Is the specified item a conjured one?
     *
     * @param item - The item to check
     * @return - true if this item is conjured, false otherwise.
     */
    boolean isConjuredItem(Item item);
}
