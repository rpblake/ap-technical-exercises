package com.example.rb.gildedrose.conjured;

import com.example.rb.gildedrose.Item;
import com.example.rb.gildedrose.ItemNames;

/**
 * Default implementation of the ConjuredItemIdentifier. Simply checks that the item
 * name is "Conjured Mana Cake". Who knows we may have more conjured items in the future!.
 */
public class DefaultConjuredItemIdentifier implements ConjuredItemIdentifier {

    @Override
    public boolean isConjuredItem(Item item) {
        return item != null && ItemNames.CONJURED_MANA_CAKE.equals(item.name);
    }
}
