package com.example.rb.gildedrose.factory;

import com.example.rb.gildedrose.Item;
import com.example.rb.gildedrose.adjusters.ItemAdjuster;
import com.example.rb.gildedrose.ItemNames;
import com.example.rb.gildedrose.adjusters.AgedBrieItemAdjuster;
import com.example.rb.gildedrose.adjusters.BackStagePassItemAdjuster;
import com.example.rb.gildedrose.adjusters.ConjuredItemAdjuster;
import com.example.rb.gildedrose.adjusters.DefaultItemAdjuster;
import com.example.rb.gildedrose.conjured.ConjuredItemIdentifier;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Our default implementation of the ItemAdjustFactory. Things are hard-coded and held in memory, but this works for our
 * MVP. Over-time we may extend this to allow the mapping of adjusters to be injected into the factory at runtime.
 *
 * The default implementation will return adjusters in the following order of precedence:
 *  - Is there a provider explicitly registered for the item name?
 *  - Is the item a conjured item from our new supplier?
 *  - Our default adjuster
 */
public class DefaultItemAdjusterFactory implements ItemAdjusterFactory {

    private final Map<String, ItemAdjuster> itemSpecificAdjusters;

    private final ConjuredItemIdentifier conjuredItemIdentifier;

    private final ItemAdjuster defaultAdjuster = new DefaultItemAdjuster();

    private final ItemAdjuster conjuredItemAdjuster = new ConjuredItemAdjuster();

    public DefaultItemAdjusterFactory(ConjuredItemIdentifier conjuredItemIdentifier) {
        Objects.requireNonNull(conjuredItemIdentifier, "conjuredItemIdentifier cannot be null");
        this.conjuredItemIdentifier = conjuredItemIdentifier;

        itemSpecificAdjusters = new HashMap<>();
        itemSpecificAdjusters.put(ItemNames.AGED_BRIE, new AgedBrieItemAdjuster());
        itemSpecificAdjusters.put(ItemNames.BACKSTAGE_PASSES, new BackStagePassItemAdjuster());
    }

    @Override
    public ItemAdjuster getAdjusterFor(Item item) {
        Objects.requireNonNull(item, "Cannot retrieve adjuster for null item object.");

        ItemAdjuster itemAdjuster = itemSpecificAdjusters.get(item.name);
        if(itemAdjuster == null) {
            itemAdjuster = conjuredItemIdentifier.isConjuredItem(item) ? conjuredItemAdjuster : defaultAdjuster;
        }
        return itemAdjuster;
    }
}
