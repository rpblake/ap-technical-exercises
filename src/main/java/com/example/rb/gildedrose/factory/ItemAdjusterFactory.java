package com.example.rb.gildedrose.factory;

import com.example.rb.gildedrose.Item;
import com.example.rb.gildedrose.adjusters.ItemAdjuster;

/**
 * A Factory interface to provide instances of ItemAdjusters for Items.
 */
public interface ItemAdjusterFactory {

    /**
     * Given the specific {@link Item} return an appropriate {@link ItemAdjuster} to help us update the sell in and
     * value of the item.
     *
     * @param item - The Item to get the ItemAdjuster for
     * @return - The ItemAdjuster.
     */
    ItemAdjuster getAdjusterFor(Item item);
}
