package com.example.rb.operator;

import com.example.rb.operator.k8s.PodDeployerResource;
import io.fabric8.kubernetes.api.model.OwnerReference;
import io.fabric8.kubernetes.api.model.OwnerReferenceBuilder;
import io.fabric8.kubernetes.api.model.apps.Deployment;
import io.fabric8.kubernetes.client.KubernetesClient;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Given an instance of our PodDeployerResource, create a Deployment resource that will deploy
 * the given number of pods with the image that we've specified. Additionally wait to ensure that
 * the Deployment Resource instance we have created is showing all replicas as running.
 */
public class DeploymentCreator {

    private final KubernetesClient kubernetesClient;

    public DeploymentCreator(KubernetesClient kubernetesClient) {
        Objects.requireNonNull(kubernetesClient, "kubernetesClient cannot be null");
        this.kubernetesClient = kubernetesClient;
    }

    public void createDeploymentFor(PodDeployerResource podDeployerResource) {

        Objects.requireNonNull(podDeployerResource, "Cannot create a Deployment for null podDeployerResource");

        Deployment deployment = loadDeployment();
        deployment.getMetadata().setName(podDeployerResource.getMetadata().getName());
        deployment.getSpec().setReplicas(podDeployerResource.getSpec().getPodCount());
        deployment.getSpec().getTemplate().getSpec().getContainers().get(0).setImage(podDeployerResource.getSpec().getPodImage());
        deployment.getMetadata().setOwnerReferences(createOwnerReference(podDeployerResource));

        deployment = kubernetesClient.apps().deployments().createOrReplace(deployment);
        waitForDeploymentToBeReady(deployment);
    }

    /*
        Check that the deployment is ready. We ensure that the number of ready replicas is equal to the number
        of requested replicas.
     */
    private boolean checkDeploymentIsReady(Deployment deployment) {
        Deployment dep = kubernetesClient.apps().deployments().withName(deployment.getMetadata().getName()).get();
        return dep.getStatus().getReadyReplicas() == dep.getStatus().getReplicas();
    }

    /*
        Wait for the number of replicas in our Deployment to be ready.
     */
    private void waitForDeploymentToBeReady(Deployment deployment) {

        while (!checkDeploymentIsReady(deployment)) {
            /*
                I mean, this is a horrible way to wait for the Deployment to be ready, but it is just for demonstration. We should
                really be watching for events from the K8S API for the Deployment Resource we've just created, and also timing
                out the creation of the Deployment resource if it doesn't complete within a given threshold.
             */
            try {
                Thread.sleep(TimeUnit.SECONDS.toMillis(10));
            } catch (InterruptedException e) {
                throw new RuntimeException("Interrupted waiting for Deployment to be ready", e);
            }
        }

    }

    /*
        Create the OwnerReference. The Deployment is "owned" by the PodDeployerResource, so if the PodDeployerResource
        is deleted, we rely on K8S in-built garbage collection to delete the related Deployment. The deletion of the
        Deployment will block the deletion of the PodDeployerResource.
     */
    private List<OwnerReference> createOwnerReference(PodDeployerResource pdr) {
        OwnerReference ref = new OwnerReferenceBuilder()
                .withApiVersion(pdr.getApiVersion())
                .withKind(pdr.getKind())
                .withName(pdr.getMetadata().getName())
                .withBlockOwnerDeletion(true)
                .withUid(pdr.getMetadata().getUid())
                .withController(true)
                .build();

        return Collections.singletonList(ref);
    }

    /*
        We store a YML representation of the Deployment on the classpath. We could equally assemble this via code, but
        easier to do it by loading a YML representation and then modifying it.
     */
    private Deployment loadDeployment() {
        try (InputStream deploymentResource = getClass().getResourceAsStream("/k8s/deployment.yml")) {
            return kubernetesClient.apps().deployments().load(deploymentResource).get();
        } catch (IOException e) {
            throw new RuntimeException("Unable to load deployment.yml from the classpath");
        }
    }
}
