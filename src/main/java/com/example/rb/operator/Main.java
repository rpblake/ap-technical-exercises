package com.example.rb.operator;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.concurrent.TimeUnit;

/**
 * A class with a main method that would be executed as the main class of our JAR. The JAR would then be built into
 * a container image with the ENTRYPOINT of the container set to execute the JAR.
 *
 * Again this is just a demonstrator of us creating a spring context and then closing it when the JVM receives a
 * SIGTERM from the operating system.
 */
public class Main {

    private static volatile boolean SHUTDOWN = false;

    public static void main(String[] args) throws Exception {

        Runtime.getRuntime().addShutdownHook(new Thread(() -> SHUTDOWN = true));
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(SpringContext.class);

        while (!SHUTDOWN) {
            Thread.sleep(TimeUnit.SECONDS.toMillis(5));
        }

        ctx.close();
    }
}