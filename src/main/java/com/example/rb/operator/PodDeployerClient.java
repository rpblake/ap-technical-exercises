package com.example.rb.operator;

import com.example.rb.operator.k8s.PodDeployerDoneable;
import com.example.rb.operator.k8s.PodDeployerResource;
import com.example.rb.operator.k8s.PodDeployerResourceList;
import io.fabric8.kubernetes.client.Watcher;
import io.fabric8.kubernetes.client.dsl.MixedOperation;
import io.fabric8.kubernetes.client.dsl.Resource;

import java.util.List;
import java.util.Objects;

/**
 * Operations to work with the PodDeployer CustomResource instances in our namespace.
 */
public class PodDeployerClient {

    private final MixedOperation<PodDeployerResource, PodDeployerResourceList, PodDeployerDoneable, Resource<PodDeployerResource, PodDeployerDoneable>> client;

    public PodDeployerClient(MixedOperation<PodDeployerResource, PodDeployerResourceList, PodDeployerDoneable, Resource<PodDeployerResource, PodDeployerDoneable>> client) {
        this.client = client;
    }

    /*
        Lists all of the currently known PodDeployerResources within our namespace.
     */
    public List<PodDeployerResource> listAll() {
        return client.list().getItems();
    }

    /*
        Registers a Watcher to receive events from the K8S API for PodDeployerResources in this namespace.
     */
    public void registerWatcher(Watcher<PodDeployerResource> watcher) {
        Objects.requireNonNull(watcher, "Cannot register a null watcher");
        client.watch(watcher);
    }
}
