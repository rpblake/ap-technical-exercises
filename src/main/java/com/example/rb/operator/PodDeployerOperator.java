package com.example.rb.operator;

import com.example.rb.operator.k8s.PodDeployerResource;
import io.fabric8.kubernetes.client.KubernetesClientException;
import io.fabric8.kubernetes.client.Watcher;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
    Implements the core reconcile loop for the PodDeployer CRD. Events on an instance of
    the PodDeployer are reconciled sequentially, but with the latest event taking precedence over any pending
    reconcile.

    If the PodDeployer resource is deleted, then we interrupt (as best we can) the reconcile loop for that resource instance.
 */
public class PodDeployerOperator implements Watcher<PodDeployerResource> {

    private final PodDeployerClient podDeployerClient;

    private final DeploymentCreator deploymentCreator;

    private final Map<String, Future<?>> runningReconciles = new HashMap<>();

    private final Map<String, PodDeployerResource> toReconcile = new HashMap<>();

    private final ExecutorService executor;

    public PodDeployerOperator(PodDeployerClient podDeployerClient, DeploymentCreator deploymentCreator, ExecutorService executor) {
        this.podDeployerClient = podDeployerClient;
        this.executor = executor;
        this.deploymentCreator = deploymentCreator;
    }

    /*
        After initialisation we list all of the current PodDeployer resources in our namespace and reconcile them.
        We then add ourself as a watcher for further events from the K8S API.
     */
    public void init() {
        podDeployerClient.listAll().forEach(this::scheduleReconcile);
        podDeployerClient.registerWatcher(this);
    }

    public void destroy() {
        executor.shutdown();
    }

    /*
        Watch for Events from K8S in relation to our PodDeployer CRD instances.
     */
    @Override
    public void eventReceived(Action action, PodDeployerResource podDeployerResource) {
        switch (action) {
            case ADDED:
            case MODIFIED:
                scheduleReconcile(podDeployerResource);
                break;
            case DELETED:
                cancelReconcile(podDeployerResource);
                break;
        }
    }

    /*
        On shutdown, cancel any on-going reconciles that we might be doing.
     */
    private synchronized void cancelReconcile(PodDeployerResource podDeployerResource) {
        Future<?> runningReconcile = runningReconciles.remove(podDeployerResource.getMetadata().getUid());
        if (runningReconcile != null) {
            runningReconcile.cancel(true);
        }

        toReconcile.remove(podDeployerResource.getMetadata().getUid());
    }

    /*
        Complete the current reconcile for the given resource and if an event has been received during
        our reconcile, schedule that for execution.
     */
    private synchronized void completeReconcile(PodDeployerResource podDeployerResource) {
        runningReconciles.remove(podDeployerResource.getMetadata().getUid());
        PodDeployerResource nextReconcile = toReconcile.remove(podDeployerResource.getMetadata().getUid());
        if (nextReconcile != null) {
            scheduleReconcile(podDeployerResource);
        }
    }

    /*
        Schedule the reconcile for our PodDeployerResource. Events can be received whilst we're
        in the process of already reconciling the resource. We therefore let the currently executing
        reconcile complete before attempting the next one. If multiple events are received whilst
        we're reconciling, then we simply keep the last event as this is the most recent desired
        "state of the world".
     */
    private synchronized void scheduleReconcile(PodDeployerResource podDeployerResource) {
        if (runningReconciles.containsKey(podDeployerResource.getMetadata().getUid())) {
            toReconcile.put(podDeployerResource.getMetadata().getUid(), podDeployerResource);
        } else {
            Future<?> future = executor.submit(() -> reconcile(podDeployerResource));
            runningReconciles.put(podDeployerResource.getMetadata().getUid(), future);
        }
    }

    private void reconcile(PodDeployerResource podDeployerResource) {
        deploymentCreator.createDeploymentFor(podDeployerResource);
        completeReconcile(podDeployerResource);
    }

    @Override
    public void onClose(KubernetesClientException e) {

    }
}
