package com.example.rb.operator;

import com.example.rb.operator.k8s.PodDeployerDoneable;
import com.example.rb.operator.k8s.PodDeployerResource;
import com.example.rb.operator.k8s.PodDeployerResourceList;
import io.fabric8.kubernetes.api.model.apiextensions.v1beta1.CustomResourceDefinition;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.ConfigBuilder;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.dsl.MixedOperation;
import io.fabric8.kubernetes.client.dsl.Resource;
import io.fabric8.kubernetes.internal.KubernetesDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Spring context to build all of the bean dependencies required by our Operator.
 */
@Configuration
public class SpringContext {

    /*
        Number of threads to be used to reconcile can be injected via environment configuration, typically
        provided via a ConfigMap using envFrom:

        @see: https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/#configure-all-key-value-pairs-in-a-configmap-as-container-environment-variables
     */
    @Value("${OPERATOR_RECONCILE_THREADS}:10}")
    private int reconcileThreads;

    /*
        CRD Version can be injected via environment configuration, typically provided via a ConfigMap using envFrom:

        @see: https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/#configure-all-key-value-pairs-in-a-configmap-as-container-environment-variables
     */
    @Value("${OPERATOR_CRD_VERSION:v1beta1}")
    private String apiVersion;

    /*
        The only adjustment to the K8S client configuration is to ensure we work with the version of the CRD
        that we create. We default to the K8S in-cluster API endpoint of https://kubernetes.default.svc

        Additionally credentials for the API are taken from the ServiceAccount that is created for the Operator
        with the correct ClusterRole and ClusterRoleBinding. ServiceAccount credentials are mounted into the Pod
        at runtime.

        Finally, namespace is again inherited from the ServiceAccount credentials when they are bound into the Pod.
     */
    @Bean
    public Config config() {
        return new ConfigBuilder().withApiVersion(apiVersion).build();
    }

    @Bean
    public KubernetesClient kubernetesClient() {
        return new DefaultKubernetesClient(config());
    }

    @Bean
    public DeploymentCreator deploymentCreator() {
        return new DeploymentCreator(kubernetesClient());
    }

    @Bean
    public CustomResourceDefinition crd() {
        InputStream resource = getClass().getResourceAsStream("/crds/pod-deployer-crd.yml");
        return kubernetesClient().customResourceDefinitions().load(resource).get();
    }

    @Bean
    public MixedOperation<PodDeployerResource, PodDeployerResourceList, PodDeployerDoneable, Resource<PodDeployerResource, PodDeployerDoneable>> crdClient() {

        CustomResourceDefinition definition = crd();
        MixedOperation<PodDeployerResource, PodDeployerResourceList, PodDeployerDoneable, Resource<PodDeployerResource, PodDeployerDoneable>> pd = kubernetesClient()
                .customResources(definition, PodDeployerResource.class, PodDeployerResourceList.class, PodDeployerDoneable.class);
        KubernetesDeserializer.registerCustomKind(definition.getSpec().getGroup() + "/" + apiVersion, definition.getSpec().getNames().getKind(), PodDeployerResource.class);
        return pd;
    }

    @Bean
    public ExecutorService threadPoolExecutor() {
        return Executors.newFixedThreadPool(reconcileThreads);
    }

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public PodDeployerOperator podDeployerOperator() {
        return new PodDeployerOperator(podDeployerClient(), deploymentCreator(), threadPoolExecutor());
    }

    @Bean
    public PodDeployerClient podDeployerClient() {
        return new PodDeployerClient(crdClient());
    }


}
