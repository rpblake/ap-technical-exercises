package com.example.rb.operator.k8s;

import io.fabric8.kubernetes.api.builder.Function;
import io.fabric8.kubernetes.client.CustomResourceDoneable;

public class PodDeployerDoneable extends CustomResourceDoneable<PodDeployerResource> {
    public PodDeployerDoneable(PodDeployerResource resource, Function<PodDeployerResource, PodDeployerResource> function) {
        super(resource, function);
    }
}
