package com.example.rb.operator.k8s;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.fabric8.kubernetes.client.CustomResource;

/**
 * POJO representation of our PodDeployer CRD.
 */
public class PodDeployerResource extends CustomResource {

    @JsonProperty
    private PodDeployerSpec spec;

    public PodDeployerSpec getSpec() {
        return spec;
    }

    public void setSpec(PodDeployerSpec spec) {
        this.spec = spec;
    }
}
