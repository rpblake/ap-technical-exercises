package com.example.rb.operator.k8s;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
    Represents the spec of our PodDeployer CRD
 */
public class PodDeployerSpec {

    @JsonProperty
    private int podCount;

    @JsonProperty
    private String podImage;

    public int getPodCount() {
        return podCount;
    }

    public void setPodCount(int podCount) {
        this.podCount = podCount;
    }

    public String getPodImage() {
        return podImage;
    }

    public void setPodImage(String podImage) {
        this.podImage = podImage;
    }
}
