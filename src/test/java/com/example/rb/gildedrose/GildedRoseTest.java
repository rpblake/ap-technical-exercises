package com.example.rb.gildedrose;

import com.example.rb.gildedrose.adjusters.ItemAdjuster;
import com.example.rb.gildedrose.factory.DefaultItemAdjusterFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GildedRoseTest {

    @Mock
    private DefaultItemAdjusterFactory adjusterFactory;

    @Mock
    private ItemAdjuster adjuster;

    void setItemSellInAndQualityOnAdjust(Item item, int sellIn, int quality) {
        when(adjusterFactory.getAdjusterFor(item)).thenReturn(adjuster);
        doAnswer((Answer<Item>) invocationOnMock -> {
            Item i = invocationOnMock.getArgument(0);
            i.quality = quality;
            i.sellIn = sellIn;
            return i;
        }).when(adjuster).adjustItemQuality(item);
    }

    @Test
    void foo() {
        Item i = new Item("foo", 0, 0);
        Item[] items = new Item[]{i};
        setItemSellInAndQualityOnAdjust(i, -1, 0);

        GildedRose app = new GildedRose(items, adjusterFactory);
        app.updateQuality();
        assertEquals("foo", app.items[0].name);
        assertEquals(-1, app.items[0].sellIn);
        assertEquals(0, app.items[0].quality);
    }

    @Test
    public void updateQuality_SulfurasNeverChangesValue() {

        Item i = new Item(ItemNames.SULFURAS_HAND_OF_RAGNAROS, 45, 67);
        Item[] items = new Item[]{i};
        GildedRose app = new GildedRose(items, adjusterFactory);
        app.updateQuality();

        assertThat(i.sellIn, is(equalTo(45)));
        assertThat(i.quality, is(equalTo(67)));
    }

    @Test
    public void updateQuality_ItemMaxValueCannotExceed50() {
        Item i = new Item(ItemNames.AGED_BRIE, 5, 50);
        Item[] items = new Item[]{i};
        setItemSellInAndQualityOnAdjust(i, 4, 51);

        GildedRose app = new GildedRose(items, adjusterFactory);
        app.updateQuality();

        assertThat(i.sellIn, is(equalTo(4)));
        assertThat(i.quality, is(equalTo(50)));
    }

    @Test
    public void updateQuality_ItemMinValueCannotBeLessThanZero() {
        Item i = new Item("foo", 5, 50);
        Item[] items = new Item[]{i};
        setItemSellInAndQualityOnAdjust(i, 4, -10);

        GildedRose app = new GildedRose(items, adjusterFactory);
        app.updateQuality();

        assertThat(i.sellIn, is(equalTo(4)));
        assertThat(i.quality, is(equalTo(0)));
    }
}
