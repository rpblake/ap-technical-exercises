package com.example.rb.gildedrose.adjusters;

import com.example.rb.gildedrose.Item;
import com.example.rb.gildedrose.ItemNames;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class AgedBrieItemAdjusterTest {

    private AgedBrieItemAdjuster itemAdjuster;

    @BeforeEach
    public void before() {
        itemAdjuster = new AgedBrieItemAdjuster();
    }

    @Test
    public void adjustItemQuality_QualityIncreasesWithAge() {
        Item item = new Item(ItemNames.AGED_BRIE, 5, 5);
        itemAdjuster.adjustItemQuality(item);

        assertThat(item.sellIn, is(equalTo(4)));
        assertThat(item.quality, is(equalTo(6)));
    }
}
