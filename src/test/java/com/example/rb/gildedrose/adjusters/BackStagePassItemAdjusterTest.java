package com.example.rb.gildedrose.adjusters;

import com.example.rb.gildedrose.Item;
import com.example.rb.gildedrose.ItemNames;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class BackStagePassItemAdjusterTest {

    private BackStagePassItemAdjuster backStagePassItemAdjuster;

    @BeforeEach
    public void before() {
        backStagePassItemAdjuster = new BackStagePassItemAdjuster();
    }

    @Test
    public void adjustItemQuality_QualityIncreasesByTwoWhenSellInIsBetween10And5() {
        Item i = new Item(ItemNames.BACKSTAGE_PASSES, 9, 5);
        backStagePassItemAdjuster.adjustItemQuality(i);

        assertThat(i.sellIn, is(equalTo(8)));
        assertThat(i.quality, is(equalTo(7)));
    }

    @Test
    public void adjustItemQuality_QualityIncreasesByThreeWhenSellInIsLessThanFive() {
        Item i = new Item(ItemNames.BACKSTAGE_PASSES, 3, 5);
        backStagePassItemAdjuster.adjustItemQuality(i);

        assertThat(i.sellIn, is(equalTo(2)));
        assertThat(i.quality, is(equalTo(8)));
    }

    @Test
    public void adjustItemQuality_QualityDecreasesToZeroWhenConcertHasPassed() {
        Item i = new Item(ItemNames.BACKSTAGE_PASSES, 1, 15);
        backStagePassItemAdjuster.adjustItemQuality(i);

        assertThat(i.sellIn, is(equalTo(0)));
        assertThat(i.quality, is(equalTo(0)));
    }

}
