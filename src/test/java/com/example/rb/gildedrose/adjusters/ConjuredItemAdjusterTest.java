package com.example.rb.gildedrose.adjusters;

import com.example.rb.gildedrose.Item;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class ConjuredItemAdjusterTest {

    private ConjuredItemAdjuster conjuredItemAdjuster;

    @BeforeEach
    public void before() {
        conjuredItemAdjuster = new ConjuredItemAdjuster();
    }

    @Test
    public void adjustItemQuality_QualityDegradesTwiceAsFast() {
        Item item = new Item("Conjured Item", 5, 5);
        conjuredItemAdjuster.adjustItemQuality(item);

        assertThat(item.sellIn, is(equalTo(4)));
        assertThat(item.quality, is(equalTo(3)));
    }

}
