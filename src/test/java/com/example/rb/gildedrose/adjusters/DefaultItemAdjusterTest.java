package com.example.rb.gildedrose.adjusters;

import com.example.rb.gildedrose.Item;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class DefaultItemAdjusterTest {

    private DefaultItemAdjuster defaultItemAdjuster;

    @BeforeEach
    public void before() {
        defaultItemAdjuster = new DefaultItemAdjuster();
    }

    @Test
    public void adjustItemQuality_Default() {
        Item i = new Item("default", 5, 5);
        defaultItemAdjuster.adjustItemQuality(i);

        assertThat(i.quality, is(equalTo(4)));
        assertThat(i.sellIn, is(equalTo(4)));
    }

    @Test
    public void adjustItemQuality_QualityReducesTwiceAsFastWhenSellInIsZero() {
        Item i = new Item("default", 0, 5);
        defaultItemAdjuster.adjustItemQuality(i);

        assertThat(i.quality, is(equalTo(3)));
        assertThat(i.sellIn, is(equalTo(-1)));
    }
}
