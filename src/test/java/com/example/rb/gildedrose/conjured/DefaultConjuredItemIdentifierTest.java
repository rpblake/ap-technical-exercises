package com.example.rb.gildedrose.conjured;

import com.example.rb.gildedrose.Item;
import com.example.rb.gildedrose.ItemNames;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author rblake@redhat.com
 */
public class DefaultConjuredItemIdentifierTest {

    private DefaultConjuredItemIdentifier identifier;

    @BeforeEach
    public void before() {
        identifier = new DefaultConjuredItemIdentifier();
    }

    @Test
    public void isConjuredItem_WithAConjuredItem() {

        Item i = new Item(ItemNames.CONJURED_MANA_CAKE, 0, 0);
        assertThat(identifier.isConjuredItem(i), is(true));
    }

    @Test
    public void isConjuredItem_NotAConjuredItem() {

        Item i = new Item(ItemNames.AGED_BRIE, 0, 0);
        assertThat(identifier.isConjuredItem(i), is(false));
    }
}
