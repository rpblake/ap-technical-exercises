package com.example.rb.gildedrose.factory;

import com.example.rb.gildedrose.Item;
import com.example.rb.gildedrose.ItemNames;
import com.example.rb.gildedrose.adjusters.AgedBrieItemAdjuster;
import com.example.rb.gildedrose.adjusters.BackStagePassItemAdjuster;
import com.example.rb.gildedrose.adjusters.ConjuredItemAdjuster;
import com.example.rb.gildedrose.adjusters.DefaultItemAdjuster;
import com.example.rb.gildedrose.adjusters.ItemAdjuster;
import com.example.rb.gildedrose.conjured.ConjuredItemIdentifier;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.notNullValue;

@ExtendWith(MockitoExtension.class)
public class DefaultItemAdjusterFactoryTest {

    @Mock
    private ConjuredItemIdentifier conjuredItemIdentifier;

    @InjectMocks
    private DefaultItemAdjusterFactory itemAdjusterFactory;

    @Test
    public void getAdjusterFor_AgedBrie() {

        Item item = new Item(ItemNames.AGED_BRIE, 0, 0);
        ItemAdjuster itemAdjuster = itemAdjusterFactory.getAdjusterFor(item);
        assertThat(itemAdjuster, is(notNullValue()));
        assertThat(itemAdjuster, isA(AgedBrieItemAdjuster.class));
    }

    @Test
    public void getAdjusterFor_BackStagePasses() {

        Item item = new Item(ItemNames.BACKSTAGE_PASSES, 0, 0);
        ItemAdjuster itemAdjuster = itemAdjusterFactory.getAdjusterFor(item);
        assertThat(itemAdjuster, is(notNullValue()));
        assertThat(itemAdjuster, isA(BackStagePassItemAdjuster.class));
    }

    @Test
    public void getAdjusterFor_NotNamedItemButIsConjured() {

        Item item = new Item("A Conjured Item", 0, 0);
        Mockito.when(conjuredItemIdentifier.isConjuredItem(item)).thenReturn(true);

        ItemAdjuster itemAdjuster = itemAdjusterFactory.getAdjusterFor(item);
        assertThat(itemAdjuster, is(notNullValue()));
        assertThat(itemAdjuster, isA(ConjuredItemAdjuster.class));
    }

    @Test
    public void getAdjusterFor_Default() {

        Item item = new Item("Another Item", 0, 0);
        Mockito.when(conjuredItemIdentifier.isConjuredItem(item)).thenReturn(false);

        ItemAdjuster itemAdjuster = itemAdjusterFactory.getAdjusterFor(item);
        assertThat(itemAdjuster, is(notNullValue()));
        assertThat(itemAdjuster, isA(DefaultItemAdjuster.class));
    }
}
