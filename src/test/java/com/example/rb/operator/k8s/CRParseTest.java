package com.example.rb.operator.k8s;

import io.fabric8.kubernetes.client.utils.Serialization;
import org.junit.jupiter.api.Test;

import java.io.InputStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Small test to ensure that we can successfully parse our CRD definition.
 */
public class CRParseTest {

    @Test
    public void parseCrInstance() throws Exception {
        InputStream in = getClass().getResourceAsStream("/crs/pod-deployer.yml");
        PodDeployerResource podDeployer = Serialization.yamlMapper().readValue(in, PodDeployerResource.class);

        assertThat(podDeployer.getApiVersion(), equalTo("rb.example.com/v1beta1"));
        assertThat(podDeployer.getKind(), equalTo("PodDeployer"));
        assertThat(podDeployer.getMetadata().getName(), equalTo("anexamplepoddeployer"));
        assertThat(podDeployer.getSpec().getPodCount(), equalTo(2));
        assertThat(podDeployer.getSpec().getPodImage(), equalTo("nginx:1.14.2"));
    }
}
