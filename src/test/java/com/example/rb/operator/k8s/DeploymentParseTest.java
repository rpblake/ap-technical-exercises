package com.example.rb.operator.k8s;

import io.fabric8.kubernetes.api.model.apps.Deployment;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import org.junit.jupiter.api.Test;

import java.io.InputStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/*
    Small test to ensure that our Deployment definition is valid and parseable via the Fabric 8 client.
 */
public class DeploymentParseTest {

    @Test
    public void loadDeployment() {

        InputStream deploymentResource = DeploymentParseTest.class.getResourceAsStream("/k8s/deployment.yml");

        DefaultKubernetesClient client = new DefaultKubernetesClient();
        Deployment deployment = client.apps().deployments().load(deploymentResource).get();

        assertThat(deployment.getMetadata().getName(), equalTo("replaceThis"));
        assertThat(deployment.getSpec().getReplicas(), equalTo(0));
        assertThat(deployment.getSpec().getTemplate().getSpec().getContainers().get(0).getImage(), equalTo("replacethis"));
    }
}
